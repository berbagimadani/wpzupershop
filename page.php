<?php 
get_header();

?>
<div class="content">
        <div class="padded-full responsive-desk">
        <div class="padded-bottom"></div>
<?php while ( have_posts() ) : the_post(); ?>

	<?php if ( is_page() ) { ?>
<div class=""> 
            <h2><?php echo the_title(); ?></h2>

            <p>
            <?php echo the_content(); ?>
            </p> 
</div>
<?php } ?>
	
<?php endwhile; // end of the loop. ?>

<div class="row">  
        <div class=""></div>
    </div>
   <div class="padded-bottom"></div>

<?php get_footer(); ?>