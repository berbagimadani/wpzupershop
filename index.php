<?php 
get_header();

?>
<div class="content">
        <div class="padded-full responsive-desk">
        <div class="padded-bottom"></div>
    <?php if (have_posts()) : ?>

        <h1><?php bloginfo('description'); ?>  <?php if ( get_query_var('paged') ) { echo ' ('; echo _e('Halaman') . ' ' . get_query_var('paged');   echo ')';  } ?></h1>

        <?php

        $i=0; 
        while ( have_posts() ) : the_post(); 
            if($post->post_type=='post'&&get_post_meta($post->ID, "harga", true)!='') :
        ?>       
                        <div class=""><div class="phone-12 tablet-6 column custome-grid-box" style=""> 
                            <a href="<?php echo get_permalink(); ?>">
                            <?php
                                $label = "";
                                if( get_post_meta($post->ID, "label", true) == "best") {
                                    $label = "Best Seller";
                                }
                                if( get_post_meta($post->ID, "label", true) == "sale") {
                                    $label = "Sale";
                                }
                                if( get_post_meta($post->ID, "label", true) == "new") {
                                    $label = "New";
                                }
                                if( get_post_meta($post->ID, "label", true) == "limited") {
                                    $label = "Limited";
                                }
                            ?>
                            <?php if( get_post_meta($post->ID, "habis", true) == false) { ?>
                            <div class="ribbon <?php echo get_post_meta($post->ID, "label", true); ?>"><span><?php echo $label; ?></span></div>
                            <?php } ?>
                            <?php if( get_post_meta($post->ID, "habis", true) ) { ?>
                            <div class="ribbon sold"><span>Habis</span></div>
                            <?php } ?>

                            <div class="custome-box-images-thumb">
                                <?php if (has_post_thumbnail( $post->ID ) ): ?>
                                    
                                    <?php zupershop_thumb(); //echo the_post_thumbnail('thumbnail', array('class' => 'wpshopguardian-res-img')); ?> 

                                <?php else: ?>
                                    <img src="http://via.placeholder.com/245x245" class="wpshopguardian-res-img">
                                <?php endif; ?>
                                
                            </div>
                            <h4><?php the_title() ?></h4>
                            </a>
                            <h5 class="zuper-price">Rp. <?php echo get_post_meta($post->ID, "harga_diskon", true); ?>
                                <span class="zuper-price-discount"><?php echo get_post_meta($post->ID, "harga", true); ?></span>
                            </h5>

                            <div class="text-center">
                            <a href="<?php echo get_permalink(); ?>" class="">
                                <button class="btn primary"><i class="icon icon-info-outline"></i> Details</button>
                            </a>
                            </div>
                        </div> </div>
             
        <?php endif; endwhile; ?>
        

        <?php 
    endif;

    ?>
    <div class="row">  
        <div class=""></div>
    </div>
   <div class="padded-bottom"></div>

 
<?php get_footer(); ?>