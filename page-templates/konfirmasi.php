<?php 
/*
Template Name: Konfirmasi
*/
get_header();

?>
<div class="content">
        <div class="padded-full responsive-desk">
        <div class="padded-bottom"></div>
        
        <h2>Konfirmasi Pembayaran</h2>
         <h3><?php echo !empty($_GET['conf']) ? 'Konfirmasi Berhasil' : ''; ?></h3>
        <form enctype="multipart/form-data" action="<?php echo get_site_url().'/Konfirmasi'; ?>" method="post" >
                    <input type="hidden" name="action" value="submit" />
                    <div class="row">

                        <div class="phone-12 tablet-12 column">
                            <?php $invalid = !empty($_GET['msg']) ? 'input-invalid' : ''; ?>
                            <div class="error">
                                <h3 style="color:red"><?php echo !empty($_GET['msg']) ? 'Form Data tidak boleh ada yang kosong' : ''; ?></h3>
                            </div> 
                            <div class="input-wrapper">
                                <input class="with-label" type="text" id="input-1" name="fullname"  value="Test">
                                <label class="floating-label" for="input-1">Nama</label>
                            </div> 
                            <div class="input-wrapper">
                                <input class="with-label" type="text" id="input-3" name="phone"  value="Test">
                                <label class="floating-label" for="input-3">No Hp</label>
                            </div>
                            <div class="input-wrapper">
                                <input class="with-label" type="text" id="input-4" name="invoice"  value="Test">
                                <label class="floating-label" for="input-4">Nomor Invoice</label>
                            </div> 
                            <div class="input-wrapper">
                                <input class="with-label" type="text" id="input-9" name="jumlah_transfer"  value="Test">
                                <label class="floating-label" for="input-9">Jumlah Transfer</label>
                            </div>  
                            <div class="input-wrapper"> 
                                <select id="pilih_kurir" name="bank_tujuan_transfer" class="custome-dropdown">
                                    <option value=""> -- Bank Tujuan Transfer -- </option>
                                    <option value="mandiri">Mandiri</option>
                                    <option value="bca">BCA</option> 
                                </select>
                            </div> 
                            <div class="input-wrapper">
                                <input class="with-label" type="text" id="input-5" name="bank_asal_transfer"  value="Test">
                                <label class="floating-label" for="input-5">Nama Bank Asal</label>
                            </div>
                            <div class="input-wrapper">
                                <input class="with-label" type="text" id="input-6" name="atas_nama"  value="Test">
                                <label class="floating-label" for="input-6">Nama Rekening Pengirim</label>
                            </div>
                            <div class="input-wrapper-">
                                <div class="padded-bottom"></div> 
                                <!-- MAX_FILE_SIZE must precede the file input field -->
                                <input type="hidden" name="MAX_FILE_SIZE" value="90000000" />
                                <!-- Name of input element determines name in $_FILES array -->
                                Upload Bukti Transfer: <input name="userfile" type="file" /> 
                            </div> 
                            <div class="input-wrapper">
                                <button class="btn fit-parent primary" type="submit">Submit Konfirmasi</button>
                            </div>
                        </div> 
                    
                    </div>
                    </form>


    <div class="row">  
        <div class=""></div>
    </div>
   <div class="padded-bottom"></div>

 
<?php
    if (isset($_POST['action'])) {

        $nama                           = $_POST['fullname'];
        $phone                          = $_POST['phone'];
        $invoice                        = $_POST['invoice'];
        $jumlah_transfer                = $_POST['jumlah_transfer'];
        $bank_tujuan_transfer           = $_POST['bank_tujuan_transfer'];
        $bank_asal_transfer             = $_POST['bank_asal_transfer'];

        $bank_tujuan_transfer = $_POST['bank_tujuan_transfer'];

        $imageupload = new File_Upload();
        $attachment_id = $imageupload->create_attachment();
        $image_url = null;
        if ( is_wp_error( $attachment_id ) ) {
            echo '<ol>';
            foreach ( $attachment_id->get_error_messages() as $err )
                printf( '<li>%s</li>', $err );
            echo '</ol>';
        } else {
            // check if the upload was successfull
            $attachment = get_post( $attachment_id );
            $image_url  = $attachment->guid;
            //printf( '<p><img src="%s"></p>', $image_url );
        }
        

        $to = get_option('email');
        $subject = 'Konfirmasi';
        $message = '
            Nama                    : '. $nama . '<br>
            Phone                   : '. $phone. '<br>
            Invoice                 : '. $invoice. '<br>
            Jumlah Transfer         : '. $jumlah_transfer.'<br>
            Bank Tujuan Transfer    : '. $bank_tujuan_transfer. '<br
            Bank Asal Transfer      : '. $bank_asal_transfer. '<br>
            Bukti transafer         : "<a href='.$image_url.'>'.$image_url.'</a>"'; 
        wp_mail( $to, $subject, $message );

        wp_redirect(get_site_url().'/konfirmasi?conf=1');

    }
?>
<?php get_footer(); ?>