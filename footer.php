
        <!-- contact -->
                    <div class="row">
                        <h3>Kontak Informasi</h3>
                        <?php if(get_option('tlp')){?>
                        <div class="tablet-6 column">
                            <ul class="list custome-hover-li"> 
                                <a href="tel:<?php echo get_option('tlp'); ?>">
                                <li class="item-expanded">  
                                    <span class="pull-left fa fa-phone" aria-hidden="true"  style="font-size:50px"></span>
                                    <div class="item-content">
                                        <span class="body">TELP</span>
                                        <span class="title"><?php echo get_option('tlp'); ?></span>
                                    </div>
                                </li> 
                                </a>
                            </ul>
                        </div>
                        <?php } ?>
                        <?php if(get_option('sms')){?>
                        <div class="tablet-6 column">
                            <ul class="list custome-hover-li"> 
                                <a href="sms:<?php echo get_option('sms'); ?>">
                                <li class="item-expanded"> 
                                    <span class="pull-left fa fa-mobile" style="font-size:50px"></span>
                                    <div class="item-content">
                                        <span class="body">SMS</span>
                                        <span class="title"><?php echo get_option('sms'); ?></span>
                                    </div>
                                </li> 
                                </a>
                            </ul>
                        </div> 
                        <?php } ?>
                    </div>
                    <div class="padded-bottom"></div>
                    <div class="row">
                        <?php if(get_option('whatsapp_number')){?>
                        <div class="tablet-6 column">
                            <ul class="list custome-hover-li">
                                <a href="https://api.whatsapp.com/send?phone=<?php echo get_option('whatsapp_number'); ?>">
                                <li class="item-expanded">  
                                    <span class="pull-left fa fa-whatsapp" style="font-size:50px"></span>
                                    <div class="item-content">
                                        <span class="body">WHATSAPP</span>
                                        <span class="title"><?php echo get_option('whatsapp_number'); ?></span>
                                    </div>
                                </li>
                                </a>
                            </ul>
                        </div>
                        <?php } ?>
                        <?php if(get_option('line')){?>
                        <div class="tablet-6 column">
                            <ul class="list custome-hover-li"> 
                                <a href="#action">
                                <li class="item-expanded"> 
                                    <span class="pull-left fa fa-whatsapp-" style="font-size:50px">
                                     <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="40px" height="70px" viewBox="0 0 300 240">
                                      <defs>
                                        <style>
                                          .fill_1 {fill: #00C300;}
                                          .fill_2 {fill: #fff;}
                                        </style>
                                      </defs>
                                      <g>
                                        <path class="fill_1" d="M245.741,0L54.679,0C24.677-0.042,0.043,24.249,0,54.257v191.057 c-0.042,30.01,24.255,54.645,54.258,54.686H245.32c30.01,0.042,54.637-24.249,54.68-54.265l-0.001-191.06 C300.041,24.671,275.749,0.041,245.741,0z"/>
                                        <path class="fill_2" d="M259.877,136.778c0-48.99-49.115-88.846-109.482-88.846 c-60.365,0-109.482,39.856-109.482,88.846c0,43.917,38.949,80.702,91.563,87.659c3.563,0.769,8.416,2.354,9.644,5.4 c1.104,2.766,0.724,7.1,0.354,9.896c0,0-1.279,7.727-1.562,9.375c-0.476,2.767-2.201,10.823,9.483,5.901 c11.686-4.922,63.047-37.127,86.016-63.568h-0.005C252.273,174.045,259.877,156.385,259.877,136.778z M107.537,165.925H85.785 c-3.164,0-5.74-2.575-5.74-5.744v-43.505c0-3.166,2.576-5.741,5.74-5.741c3.166,0,5.739,2.576,5.739,5.741v37.764h16.013 c3.169,0,5.741,2.576,5.741,5.742C113.277,163.35,110.706,165.925,107.537,165.925z M130.037,160.182 c0,3.168-2.575,5.744-5.737,5.744c-3.164,0-5.739-2.575-5.739-5.744v-43.505c0-3.166,2.575-5.741,5.739-5.741 c3.162,0,5.737,2.576,5.737,5.741V160.182z M182.402,160.182c0,2.479-1.573,4.667-3.924,5.446 c-0.591,0.198-1.207,0.298-1.824,0.298c-1.791,0-3.505-0.858-4.587-2.298l-22.293-30.359v26.914 c0,3.168-2.573,5.744-5.741,5.744c-3.166,0-5.742-2.575-5.742-5.744v-43.505c0-2.474,1.579-4.662,3.924-5.445 c0.591-0.199,1.206-0.296,1.823-0.296c1.791,0,3.509,0.856,4.584,2.295l22.3,30.362v-26.917c0-3.166,2.578-5.741,5.74-5.741 c3.167,0,5.739,2.576,5.739,5.741V160.182z M217.602,132.688c3.169,0,5.742,2.576,5.742,5.743c0,3.163-2.573,5.739-5.742,5.739 h-16.008v10.27h16.008c3.164,0,5.742,2.576,5.742,5.742c0,3.168-2.578,5.744-5.742,5.744h-21.754c-3.16,0-5.74-2.575-5.74-5.744 v-21.738c0-0.007,0-0.007,0-0.013v-21.755c0-3.166,2.575-5.741,5.74-5.741h21.754c3.169,0,5.742,2.576,5.742,5.741 c0,3.166-2.573,5.741-5.742,5.741h-16.008v10.271H217.602z"/>
                                      </g>
                                    </svg>
                                    </span>

                                    <div class="item-content">
                                        <span class="body">LINE</span>
                                        <span class="title"><?php echo get_option('line'); ?></span>
                                    </div>
                                </li> 
                                </a>
                            </ul>
                        </div> 
                        <?php } ?>
                        <?php if(get_option('bbm')){?>
                        <div class="tablet-6 column">
                            <ul class="list custome-hover-li">
                                <a href="">
                                <li class="item-expanded">  
                                    <span class="pull-left fa fa-whatsapp" style="font-size:50px"></span>
                                    <div class="item-content">
                                        <span class="body">BBM Messanger</span>
                                        <span class="title"><?php echo get_option('bbm'); ?></span>
                                    </div>
                                </li>
                                </a>
                            </ul>
                        </div>
                        <?php } ?>
                    </div>
                    <!--  end contac -->

                    <div class="padded-bottom"></div>

                    <div class="row text-center">
                        <div class="phone-12 tablet-12 column">
                            Copyright © 2017 Super Shop
                        </div>
                    </div>
                </div>
            </div>
        </home>
        <!-- scripts --> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/dist/js/phonon-core.min.js"></script>

        <!-- our app config -->
        <script src="<?php echo get_template_directory_uri(); ?>/dist/js/components/side-panels.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/dist/js/components/dialogs.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/dist/js/components/preloaders.min.js"></script>
        <!--<script src="<?php echo get_template_directory_uri(); ?>/dist/js/components/awesomplete.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/dist/js/components/autocomplete.js"></script>-->
        <script src="<?php echo get_template_directory_uri(); ?>/dist/js/components/forms.js"></script>

        <script>
            phonon.options({
                navigator: {
                    defaultPage: 'home',
                    animatePages: true
                },
                i18n: null
            });

            phonon.navigator().start();
            //var awesomplete = phonon.autocomplete(document.querySelector('#example-input'), {list: '#my-list'});
        
            function printDiv() 
            {
              var divToPrint=document.getElementById('DivIdToPrint');
              var newWin=window.open('','Print-Window');
              newWin.document.open();
              newWin.document.write('<html><link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dist/css/phonon.min.css-" /><link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dist/css/theme.css" /><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');
              newWin.document.close();
              setTimeout(function(){newWin.close();},10);
            }
        </script>

        <!-- jQuery -->
        <script type="text/javascript">
            jQuery(document).ready(function(){

                $(".box-pilih-kurir").hide();

                jQuery('#provinsi').change(function(e){
                    e.preventDefault(); 
                    //Mengambil value dari option select provinsi kemudian parameternya dikirim menggunakan ajax 
                    var prov = $('#provinsi').val();

                    $.ajax({
                        type : 'GET',
                        url : '<?php echo get_site_url() ?>/keranjang?act=ajax&ch=city',
                        data :  'prov_id=' + prov,
                            success: function (data) {
                            $("#show-dropdown-city").html(data);
                        }
                    });
                });

                jQuery('#show-dropdown-city').change(function(e){
                    e.preventDefault(); 
                    $(".box-pilih-kurir").show();
                    var city = $('#show-dropdown-city').val();
                    var pilih_kurir = $('#pilih_kurir').val();
                    var total_berat = $('#total_berat').val();

                    var price_shop = $(this).data('shop'); 
                    var price_shop_currency = toRp(price_shop); //'Rp ' + (price_shop/1000).toFixed(3);
                    $("#showTotalShipping").html('Rp 0');
                    $("#showTotalPrice").html(price_shop_currency);

                    $.ajax({
                        type : 'GET',
                        url : '<?php echo get_site_url() ?>/keranjang?act=ajax&ch=ongkir',
                        data :  'city_id=' + city + '&pilih_kurir=' + pilih_kurir + '&total_berat=' + total_berat,
                            success: function (data) {
                            $("#show-ongkir").html(data);

                            /* service courire check */
                            jQuery('.service_courier').on('change', function(e){
                                e.preventDefault(); 
                                var price_shipping = $(this).data('shipping');
                                var price_shop = $(this).data('shop'); 
                                
                                var total = parseInt(price_shipping) + parseInt(price_shop); 

                                var total_currency = toRp(total); //'Rp ' + (total/1000).toFixed(3);
                                var price_shipping_currency = 'Rp ' + (price_shipping/1000).toFixed(3);

                                $("#showTotalShipping").html(price_shipping_currency);
                                $("#showTotalPrice").html(total_currency);

                            });
                            /* end service courire check */

                            //console.log(data);
                        }
                    });
                });

                jQuery('#pilih_kurir').change(function(e){
                    e.preventDefault(); 
                    //Mengambil value dari option select provinsi kemudian parameternya dikirim menggunakan ajax 
                    var city = $('#show-dropdown-city').val();
                    var pilih_kurir = $('#pilih_kurir').val();
                    var total_berat = $('#total_berat').val();

                    var price_shop = $(this).data('shop'); 
                    var price_shop_currency = toRp(price_shop); //'Rp ' + (price_shop/1000).toFixed(3);

                    $("#showTotalShipping").html('Rp 0');
                    $("#showTotalPrice").html(price_shop_currency);

                    $.ajax({
                        type : 'GET',
                        url : '<?php echo get_site_url() ?>/keranjang?act=ajax&ch=ongkir',
                        data :  'city_id=' + city + '&pilih_kurir=' + pilih_kurir + '&total_berat=' + total_berat,
                            success: function (data) {
                            $("#show-ongkir").html(data);
                             /* service courire check */
                            jQuery('.service_courier').on('change', function(e){
                                e.preventDefault(); 
                                var price_shipping = $(this).data('shipping');
                                var price_shop = $(this).data('shop'); 
                                
                                var total = parseInt(price_shipping) + parseInt(price_shop); 

                                var total_currency = toRp(total); //'Rp ' + (total/1000).toFixed(3);
                                var price_shipping_currency = 'Rp ' + (price_shipping/1000).toFixed(3);

                                $("#showTotalShipping").html(price_shipping_currency);
                                $("#showTotalPrice").html(total_currency);

                            });
                            /* end service courire check */
                        }
                    });
                });

                function toRp(angka){
                    var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
                    var rev2    = '';
                    for(var i = 0; i < rev.length; i++){
                        rev2  += rev[i];
                        if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
                            rev2 += '.';
                        }
                    }
                    return 'Rp. ' + rev2.split('').reverse().join('') + '';
                }



                /*
                    Global Loading
                */
                jQuery.ajaxSetup({
                  beforeSend: function (xhr) {
                    // This will never fire
                    phonon.preloader('#my-preloader').show();
                    $("#show-ongkir").hide();
                  },
                  complete: function(){
                    phonon.preloader('#my-preloader').hide();
                    $("#show-ongkir").show();
                  },
                });

            });
        </script>

    </body>
</html>


