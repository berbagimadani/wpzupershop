(function( $ ) {
  $(function() {

  $('[data-uk-active]').click(function(event){
          
      var id = $(this).attr('data-uk-active');

         if ( id == "after-signup" ){

            if($("#"+id).is(":checked")) {
              $("#after-signup").removeClass('uk-hidden');
              $("#after-signup").show(); 
            }
            else{
              $("#after-signup").addClass('uk-hidden');
              $("#after-signup").hide(); 
            }
          }
          

  }); 

  /**
  * date picker jquery UI ( not uikit )
  */ 
  jQuery('input[type=text][data=date]').live('hover', function() { 
        formID = jQuery(this).attr("id");
        format = jQuery(this).attr("format");
        jQuery('#'+formID).datepicker({
          dateFormat : format
        });
  });


  /**
  * color picker
  */
  $( '.cpa-color-picker' ).wpColorPicker();

  /**
  * Populate standar
  */ 

  jQuery( 'select[data="#select_populate"]' ).change( function() { 
      
      var id_select = jQuery(this).attr("id");
      var count = jQuery(this).attr("count");
      
      var option  = $( '#'+id_select ).val(); 
      

      var get_id_option = $('#'+id_select+' #'+option);

      //$('div[rel='+count+']').removeClass('uk-hidden');
    

      $( get_id_option ).attr('selected', 'selected').siblings().removeAttr('selected');  
     // $( get_id_option ).attr('disabled', 'disabled').siblings().removeAttr('disabled'); 

        var data_select = $("select[name='"+id_select+"'] option[selected=selected]").attr('data');
 

        var array = count.split(',');
        for (var i = 0; i < array.length; i++) {
            
          

          if ( data_select == array[i] ){
              $(array[i]).removeClass('uk-hidden');
              
              //$('button'+array[i]).removeClass('uk-hidden');

              $('label[name='+id_select+'][id='+array[i]+']').removeClass('uk-hidden');

            } else {
              $(array[i]).addClass('uk-hidden');

              $('label[id='+array[i]+']').addClass('uk-hidden');

              //$('button'+array[i]).addClass('uk-hidden');
               
          }

        } 
         
  });

  /*
  Dropdown with Multiple checkbox select with jQuery - May 27, 2013
  (c) 2013 @ElmahdiMahmoud
  license: http://www.opensource.org/licenses/mit-license.php
*/ 

$(".dropdown dt a").on('click', function () {
          $(".dropdown dd ul").slideToggle('fast');
      });

      $(".dropdown dd ul li a").on('click', function () {
          $(".dropdown dd ul").hide();
      });

      function getSelectedValue(id) {
           return $("#" + id).find("dt a span.value").html();
      }

      $(document).bind('click', function (e) {
          var $clicked = $(e.target);
          if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
      });


      $('.mutliSelect input[type="checkbox"]').on('click', function () {
        
          var title = $(this).closest('.mutliSelect').find('input[type="checkbox"]').val(),
              title = $(this).val() + ",";
        
          if ($(this).is(':checked')) {
              var html = '<span title="' + title + '">' + title + '</span>';
              $('.multiSel').append(html);
              $(".hida").hide();
          } 
          else {
              $('span[title="' + title + '"]').remove();
              var ret = $(".hida");
              $('.dropdown dt a').append(ret);
              
          }
      });
  
  /**
  * Populate advance
  */ 

  jQuery( 'select[data="#select_populate_advance"]' ).change( function() { 
      
      var id_select = jQuery(this).attr("id");
      var count = jQuery(this).attr("count");
      
      var option  = $( '#'+id_select ).val(); 
      

      var get_id_option = $('#'+id_select+' #'+option);

      $('div[rel='+count+']').removeClass('uk-hidden');
    

      $( get_id_option ).attr('selected', 'selected').siblings().removeAttr('selected');  
     // $( get_id_option ).attr('disabled', 'disabled').siblings().removeAttr('disabled'); 

        var data_select = $("select[name='"+id_select+"'] option[selected=selected]").attr('data');
 

        var array = count.split(',');
        for (var i = 0; i < array.length; i++) {
            
          

          if ( data_select == array[i] ){
              $(array[i]).removeClass('uk-hidden');
              
              $('button'+array[i]).removeClass('uk-hidden');

              $('label[name='+id_select+'][id='+array[i]+']').removeClass('uk-hidden');

            } else {
              $(array[i]).addClass('uk-hidden');

              $('label[id='+array[i]+']').addClass('uk-hidden');

              $('button'+array[i]).addClass('uk-hidden');
               
          }

        } 
         
  });


  /**
  * POst category by kwch
  */ 

  jQuery( 'select[name=post_category_by_kwch]' ).change( function() { 
      
     alert($(this).val());
         
  });


  });
})( jQuery );
