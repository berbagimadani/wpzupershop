<?php
session_start();
ob_start();
/**
 * ZuperShopfunctions and definitions.
 * @package WordPress
 * @subpackage zupershop V1
 * @since zupershop v1
 * @web kabarharian.com
 * @email genthemes@gmail.com
 */

  
if ( ! isset( $content_width ) )
	$content_width = 604;
	$root = get_template_directory_uri(); 
 
require get_template_directory() . '/inc/yamm-nav-walker.php'; 
require_once  get_template_directory() . '/inc/custome_meta_box.php';
require_once  get_template_directory() . '/inc/class-wpviddycpa-field.php';
require_once  get_template_directory() . '/inc/walk.php';
require_once  get_template_directory() . '/inc/theme-options.php';
require_once  get_template_directory() . '/inc/img_resizer.php';

/* cart */
require_once  get_template_directory() . '/inc/cart/class.Cart.php';
require_once  get_template_directory() . '/inc/cart/Shop.Guardian.php';
require_once  get_template_directory() . '/inc/file_upload.php';

// Initialize Cart object
$cart = new Cart([
	  	// Can add unlimited number of item to cart
		'cartMaxItem'	=> 0,
	  
	  	// Set maximum quantity allowed per item to 99
	  	'itemMaxQuantity'	=> 99,
	  
	  	// Do not use cookie, cart data will lost when browser is closed
	  	'useCookie'	=> false,
]);
function wpshopguardian_scripts_styles() {
	global $root;
}
add_action( 'wp_enqueue_scripts', 'wpshopguardian_scripts_styles' );

function wpdocs_enqueue_custom_admin_style() {
    global $post;

    wp_enqueue_style('-uikit-styles', get_template_directory_uri() .'/inc/css/uikit.min.css', false, 'all' );
	wp_enqueue_style('-base-styles', get_template_directory_uri() .'/inc/css/base.css', false, 'all' );
	wp_enqueue_style('-date-styles', get_template_directory_uri() .'/inc/css/datepicker.min.css', false, 'all' );
	wp_enqueue_style( 'wp-color-picker' );

	wp_enqueue_script('media-upload');
	wp_enqueue_style('thickbox');
	wp_enqueue_script('-uikit-script', get_template_directory_uri() .'/inc/js/uikit.min.js', array( 'jquery', 'jquery-ui-tabs', 'wp-color-picker' ), 'all' );
	wp_enqueue_script('-core-script', get_template_directory_uri() .'/inc/js/core.js', array( 'jquery' ), 'all' );
	wp_enqueue_script('-custome-script', get_template_directory_uri() .'/inc/js/custome.js', array( 'jquery' ), 'all' );
	wp_enqueue_script('-media-upload-script', get_template_directory_uri() .'/inc/js/media-upload.js', array( 'jquery' ), 'all' );
 		/* component */ 
	wp_enqueue_script('-date-script', ( 'http://getuikit.com/src/js/components/datepicker.js' ), array( 'jquery' ), 'all' );

	/*  conflict meda-upload-js *Uncaught TypeError: Cannot read property 'frames' of * */
	wp_enqueue_media(array(
		'post' => $post,
	));

}
add_action( 'admin_enqueue_scripts', 'wpdocs_enqueue_custom_admin_style' );

 
function wpshopguardian_setup() { 
	  
	load_theme_textdomain( '', get_template_directory() . '/languages' );
	add_editor_style();
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-formats', array( 'aside', 'image', 'link', 'quote', 'status' ) );
	register_nav_menu( 'primary', __( 'Primary Menu', '' ) );
	register_nav_menu( 'one-page-menu', __( 'One Page Menu', '' ) );
	add_theme_support( 'custom-background', array(
		'default-color' => 'e6e6e6',
	) );

	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 604, 270, true );
	if ( function_exists( 'add_image_size' ) ) { 
		add_image_size( 'sidebar-thumb', 150, 150, true ); // Hard Crop Mode
		add_image_size( 'category-thumb', 250); // Soft Crop Mode 
		add_image_size( 'relatedpost-thumb', 180, 160 );
	}

	 
}
add_action( 'after_setup_theme', 'wpshopguardian_setup' );
 
if ( ! function_exists( 'wpshopguardian_comment' ) ) :
function wpshopguardian_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
		// Display trackbacks differently than normal comments.
	?>
	<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
		<p><?php _e( 'Pingback:', '' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( '(Edit)', '' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
		// Proceed with normal comments.
		global $post;
	?>
	 <!--{testing}-->

			<?php if ( '0' == $comment->comment_approved ) : ?>
				<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', '' ); ?></p>
			<?php endif; ?>  
	<?php
		break;
	endswitch; // end comment_type check
}
endif;
function wpshopguardian_enqueue_comments_reply() {
	if( get_option( 'thread_comments' ) )  {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'comment_form_before', 'wpshopguardian_enqueue_comments_reply' );

// Setting body background css
function getOption($var){
	$options = get_option('wpviddycpa-option');
    if(!empty($var))
        return @$options[$var];
}
 
if ( ! function_exists( 'wpshopguardian_content_nav' ) ) :
function wpshopguardian_content_nav( $html_id ) {
	global $wp_query;   
	if ( $wp_query->max_num_pages > 1 ) : ?>
	<ul class="pagination">
		<?php 
				$big = 999999999; // need an unlikely integer 
				echo paginate_links( array(
					'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					'format' => '?paged=%#%',
					'current' => max( 1, get_query_var('paged') ),
					'total' => $wp_query->max_num_pages
				) ); 
		?>
	</ul>
	<?php endif;
} 
endif;

/*  View post 
/* ------------------------------------ */	
if ( ! function_exists( 'wpb_set_post_views' ) ) {
	function wpb_set_post_views($postID) {
	    $count_key = 'wpb_post_views_count';
	    $count = get_post_meta($postID, $count_key, true);
	    if($count==''){
	        $count = 0;
	        delete_post_meta($postID, $count_key);
	        add_post_meta($postID, $count_key, '0');
	    }else{
	        $count++;
	        update_post_meta($postID, $count_key, $count);
	    }
	}
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

if ( ! function_exists( 'view_count' ) ) {
	function view_count($postID){
	    $count_key = 'wpb_post_views_count';
	    $count = get_post_meta($postID, $count_key, true);
	    if($count==''){
	        delete_post_meta($postID, $count_key);
	        add_post_meta($postID, $count_key, '0');
	        return "0";
	    }
	    return $count;
	}
}
if ( ! function_exists( 'wpb_track_post_views' ) ) {
	function wpb_track_post_views ($post_id) {
	    if ( !is_single() ) return;
	    if ( empty ( $post_id) ) {
	        global $post;
	        $post_id = $post->ID;    
	    }
	    wpb_set_post_views($post_id);
	}
}
//add_action( 'wp_head', 'wpb_track_post_views'); 

/*
 * 
 * CUSTOME PAGINATION
 */
function custom_pagination() {
    global $wp_query;
    $big = 999999999; // need an unlikely integer
    $pages = paginate_links( array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '?paged=%#%',
            'current' => max( 1, get_query_var('paged') ),
            'total' => $wp_query->max_num_pages,
            'prev_next' => false,
            'type'  => 'array',
            'prev_next'   => TRUE,
			'prev_text'    => __('<<'),
			'next_text'    => __('>>'),
        ) );
        if( is_array( $pages ) ) {
            $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
            echo '<ul class="pagination">';
            foreach ( $pages as $page ) {
                    echo "<li >$page</li>";
            }
           echo '</ul>';
        }
}
/*
 * 
 * CUSTOME BREADCRUMB
 */
function custom_breadcrumb() {
	if (!is_home()) { 
		if (is_category() || is_single()) {
			echo  '<li>'.the_category(' ').'</li>';
			echo '<li>'.get_the_title().'</li>';
		}	 
	}
}

function getSizeImage($attr) {
	$image_id = get_post_thumbnail_id();
	$image_url = wp_get_attachment_image_src($image_id,$attr, true);
	$link = '<a href="'.$image_url[0].'">'.$image_url[1]." x ".$image_url[2].'</a>';
	return $link;
}

function the_titlesmall($before = '', $after = '', $echo = true, $length = false) { $title = get_the_title();

	if ( $length && is_numeric($length) ) {
		$title = substr( $title, 0, $length );
	}

	if ( strlen($title)> 0 ) {
		$title = apply_filters('the_titlesmall', $before . $title . $after, $before, $after);
		if ( $echo )
			echo $title;
		else
			return $title;
	}
}
 

/* shop */
// menampilkan jumlah diskon
function wpshopguardian_diskon() { 
		global $post; 
		if(get_post_meta($post->ID, "harga_diskon", $single = true) != ""){
			$harga = get_post_meta($post->ID, "harga",$single = true);
			$harga = preg_replace('/[^0-9]/', '', $harga);
			$hargadiskon = get_post_meta($post->ID, "harga_diskon",$single = true);
			$hargadiskon = preg_replace('/[^0-9]/', '', $hargadiskon);
			$hemat = $harga - $hargadiskon;
			$jadihemat = number_format( $hemat ,0 , ',','.' );
			$save =  $hemat / $harga ;
			$persen = $save * 100 ;
			$jadipersen = number_format( $persen ,2 );
			echo $jadihemat.' ('.$jadipersen.'%)';
		}
}
function currency($price) {
	return number_format( $price ,0 , ',','.' );
}
// menampilkan jumlah diskon pada lable thumb
function wpshopguardian_diskon_label() { 
	global $post; 
		if(get_post_meta($post->ID, "harga_diskon", $single = true) != ""){
			$harga = get_post_meta($post->ID, "harga",$single = true);
			$harga = preg_replace('/[^0-9]/', '', $harga);
			$hargadiskon = get_post_meta($post->ID, "harga_diskon",$single = true);
			$hargadiskon = preg_replace('/[^0-9]/', '', $hargadiskon);
			$hemat = $harga - $hargadiskon;
			$jadihemat = number_format( $hemat ,0 , ',','.' );
			$save =  $hemat / $harga ;
			$persen = $save * 100 ;
			$jadipersen = number_format( $persen ,0 );
			echo $jadipersen.'%';
		}
}

function getProvinsi(){
	$curl = curl_init();
 
    curl_setopt_array($curl, array(
      CURLOPT_URL => "http://api.rajaongkir.com/starter/province",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "content-type: application/x-www-form-urlencoded",
        "key: ".get_option('guard_rajaongkir_key')
      ),
    ));
 
    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
	if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        return $response;
    } 
}

/*
Email
*/
function wpse27856_set_content_type(){
    return "text/html";
}
add_filter( 'wp_mail_content_type','wpse27856_set_content_type' );


function custom_mails($args){
	$cc_email = sanitize_email(get_option('email'));
	if (is_array($args['headers'])) 
	{
	$args['headers'][] = 'cc: '.$cc_email;
	}
	else 
	{
	$args['headers'] .= 'cc: '.$cc_email."\r\n";
}
return $args;
}
add_filter('wp_mail','custom_mails', 10,1);
/* end*/

function getorigin($origin=null){
	//Get Data Kabupaten
	$curl = curl_init();	
	curl_setopt_array($curl, array(
	  CURLOPT_URL => "http://api.rajaongkir.com/starter/city",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
	    "key: ".get_option('guard_rajaongkir_key')
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);
 
	echo "<select name='origin' id='origin'>";
	echo "<option>Pilih Kota Asal</option>";
		$data = json_decode($response, true);
		for ($i=0; $i < count($data['rajaongkir']['results']); $i++) { 
			$sel="";
			if($origin == $data['rajaongkir']['results'][$i]['city_id'] ){
				$sel = 'selected';
			}
		    echo "<option value='".$data['rajaongkir']['results'][$i]['city_id']."' ".$sel.">".$data['rajaongkir']['results'][$i]['city_name']."</option>";
		}
	echo "</select><br><br><br>";
	//Get Data Kabupaten
}

function zupershop_thumb() {
 	$image_id = get_post_thumbnail_id();
	$image_url = wp_get_attachment_url($image_id,'full' );
	$image = aq_resize( $image_url, 245, 245, true , false , true  ); ?>
	<img class="wpshopguardian-res-img" src="<?php echo $image[0] ?>" data-original="<?php echo $image[0] ?>" alt="<?php the_title(); ?>" width="<?php echo $image[1] ?>" height="<?php echo $image[2] ?>"/>
<?php 
	} 
?>



