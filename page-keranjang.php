<?php
if (!@$_GET['act'] == 'ajax') {
/*
Template Name: Keranjang
*/
?>
<?php get_header();  $allItems = $cart->getItems(); ?> 

<div class="content">
                <div class="padded-full responsive-desk">
                    
                    <?php
                        //print_r($allItems); 
                        //$cart->remove('payment');
                    ?>
                    <?php if(!is_array($allItems['payment'])) { ?>

                    <h3>Keranjang</h3>
                    
                    
                    <?php if (!$cart->isEmpty()) { ?>

                    <!-- include table cart item product -->
                    <?php tableCartItem(); ?>

                    <div class="padded-bottom"></div> 
                    <form action="<?php echo get_site_url().'/keranjang'; ?>" method="post">
                    <input type="hidden" name="action" value="submit" />
                    <input type="hidden" name="berat" value="<?php echo totalBerat(); ?>" />
                    <div class="row">

                        <div class="phone-12 tablet-12 column">
                            <?php $invalid = !empty($_GET['msg']) ? 'input-invalid' : ''; ?>
                            <div class="error">
                                <h3 style="color:red"><?php echo !empty($_GET['msg']) ? 'Form Data tidak boleh ada yang kosong' : ''; ?></h3>
                            </div>
                            <h3>Data Pengiriman</h3> 
                            <div class="input-wrapper">
                                <input class="with-label" type="text" id="input-1" name="fullname" required value="Ade Iskandar">
                                <label class="floating-label" for="input-1">Nama</label>
                            </div> 
                            <div class="input-wrapper">
                                <input class="with-label" type="email" id="input-2" name="email" required value="genthemes@gmail.com">
                                <label class="floating-label" for="input-2">Email</label>
                            </div> 
                            <div class="input-wrapper">
                                <input class="with-label" type="text" id="input-3" name="phone" required value="0898989">
                                <label class="floating-label" for="input-3">No Hp</label>
                            </div>
                            <div class="input-wrapper">
                                <input class="with-label" type="text" id="input-4" name="address" required value="Test alamat">
                                <label class="floating-label" for="input-4">Alamat</label>
                            </div> 
                            <div class="input-wrapper-">
                                <br>
                                <?php
                                $data = json_decode(getProvinsi(), true);
                                $statusProv = $data['rajaongkir']['status'];
                                if($statusProv['code']==400) {
                                    echo ($statusProv['description']);
                                } else {
                                echo "<select name='provinsi' id='provinsi' class='custome-dropdown'>";
                                echo "<option value=''>Pilih Provinsi Tujuan</option>"; 
                                for ($i=0; $i < count($data['rajaongkir']['results']); $i++) {
                                    echo "<option value='".$data['rajaongkir']['results'][$i]['province_id']."'>".$data['rajaongkir']['results'][$i]['province']."</option>";
                                }
                                echo "</select>*<small>required</small><br>";
                                }
                                ?> 
                            </div>
                            <div class="input-wrapper-">
                                <br>
                               <select id="show-dropdown-city" name="city" class="custome-dropdown" data-shop="<?php echo $cart->getAttributeTotal('price'); ?>" required>
                                   <option>Pilih Kota Tujuan</option>
                               </select>*<small>required</small>
                            </div>
                            <div class="input-wrapper-">
                                <br>
                                <input type="text" placeholder="Input Kecamatan" id="example-input" name="kecamatan" required value="test kecamatan">
                                <datalist id="my-list">
                                    <option></option> 
                                </datalist>
                            </div>
                            <div class="input-wrapper">
                                <input class="with-label" type="text" id="input-5" name="kode_pos" required value="5555">
                                <label class="floating-label" for="input-5">Kode Pos</label>
                            </div>
                            <!-- table kurir -->
                            <div class="box-pilih-kurir">
                                <h5><b>Pilihan Kurir</b></h5> 
                                <select id="pilih_kurir" name="pilih_kurir" class="custome-dropdown" data-shop="<?php echo $cart->getAttributeTotal('price'); ?>">  
                                   <option value="jne">Jne</option>
                                   <option value="tiki">Tiki</option>
                                   <option value="pos">POS</option>
                                </select>
                            </div>
                            <!-- end -->
                        </div> 
                        <div class="phone-12 tablet-12 column">
                            
                            <!-- show ongkir table -->
                            <div class="show-loading"></div>
                            <div id="show-ongkir"></div>
                            <!-- end -->

                            <div class="input-wrapper">
                                <textarea class="with-label" type="text" id="input-6"></textarea>
                                <label class="floating-label" for="input-6">Info Tambahan</label>
                            </div> 

                            <div class="padded-bottom"></div> 
                            <h3>Detail Pesanan</h3>
                            <div class="row">
                                <div class="phone-6 tablet-6 column"><h4>Belanja</h4></div>
                                <div class="phone-6 tablet-6 column">
                                    <?php echo 'Rp '.number_format($cart->getAttributeTotal('price'), 0, ',', '.').'';?>
                                </div>
                            </div>
                            <div class="row"> 
                                <div class="phone-6 tablet-6 column"><h4>Bea Kirim</h4></div>
                                <div class="phone-6 tablet-6 column">
                                    <div id="showTotalShipping">0</div>
                                </div> 
                            </div>
                            <div class="row">
                                <div class="phone-6 tablet-6 column"><h4>Total</h4></div>
                                <div class="phone-6 tablet-6 column">
                                    <div id="showTotalPrice">
                                        <?php echo 'Rp '.number_format($cart->getAttributeTotal('price'), 0, ',', '.').'';?>
                                    </div>
                                </div>
                            </div>
                            <div class="padded-bottom"></div>  
                            <?php
                                //print_r(getTotalShipping('457', '3700', 'jne', 'CTCYES'));
                            ?>
                            <button class="btn fit-parent primary" type="submit"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Submit Order</button>
                                
                        </div>
                    </div>
                    </form>
                    
                    <?php }  else { ?>
                    <h4>Tidak ada produk</h4>
                    Silahkan lanjutkan belanja
                    <?php } ?> 

                    <?php } ?>

                    
                    <!-- payment page -->
                    
                    <?php if(is_array($allItems['payment'])) { ?>
                    <div id='DivIdToPrint'>
                        <div class="padded-bottom"></div>
                        
                        <?php $datas = $allItems['payment'][0]; ?>
                        <h3>Terima Kasih, <?php print_r($datas['attributes']['fullname']); ?>
                            <span class="pull-right"><button onclick='printDiv();'>Print</button></span>
                        </h3>

                        *detail pesanan anda juga terkirim ke email <?php print_r($datas['attributes']['email']); ?> (Periksa Spam folder, jika email tidak masuk inbox)
                        
                        <div class="custome-list">
                                <ul class="list"> 
                                    <li class="item-expanded"> 
                                        <a class="pull-left">Invoice Id</a>
                                        <div class="item-content">
                                            <span class="title"><strong><?php print_r($datas['attributes']['invoice']); ?></strong></span> 
                                        </div>
                                    </li>
                                    <li class="item-expanded"> 
                                        <a class="pull-left">Nama</a>
                                        <div class="item-content">
                                            <span class="title"><strong><?php print_r($datas['attributes']['fullname']); ?></strong></span> 
                                        </div>
                                    </li>
                                    <li class="item-expanded"> 
                                        <a class="pull-left">Alamat</a>
                                        <div class="item-content">
                                            <span class="title"><strong><?php print_r($datas['attributes']['address']); ?></strong></span> 
                                        </div>
                                    </li>
                                    <li class="item-expanded"> 
                                        <a class="pull-left">Kode Pos</a>
                                        <div class="item-content">
                                            <span class="title"><strong><?php print_r($datas['attributes']['kode_pos']); ?></strong></span> 
                                        </div>
                                    </li>
                                    <li class="item-expanded"> 
                                        <a class="pull-left">Phone</a>
                                        <div class="item-content">
                                            <span class="title"><strong><?php print_r($datas['attributes']['phone']); ?></strong></span> 
                                        </div>
                                    </li>
                                    <li class="item-expanded"> 
                                        <a class="pull-left">Total Belanja</a>
                                        <div class="item-content">
                                            <span class="title"><strong><?php print_r($datas['attributes']['total_belanja']); ?> </strong></span> 
                                        </div>
                                    </li> 
                                </ul>
                        </div>

                        <h3>Silahkan Melakukan Pembayaran Sebesar Rp <?php print_r($datas['attributes']['total_belanja']); ?> Ke Nomer Rekening Di Bawah Ini</h3>
                        <div class="zuper-info-payment">
                            <?php echo get_option('info'); ?>
                        </div>
                        
                        <div class="zuper-info-confirm">
                            <h3>KONFIRMASI VIA WA <a href="https://api.whatsapp.com/send?phone=<?php echo get_option("whatsapp_number"); ?>">Klik Disini</a></h3> 
                            <h4>Kami Akan Segera Proses Pesanan Anda. <br><br></h4>
                            <h4>Regards, Admin <?php echo bloginfo() ?></h4>
                        </div>
                    </div>
                    <?php } ?>
                    
                    <!-- payment page  -->


                    <!-- loading -->
                    <div class="circle-progress center fixed" id="my-preloader">
                         <div class="spinner"></div>
                    </div>
                    <!-- loading -->
                    <div class="padded-bottom"></div> 

<?php get_footer(); ?>
<?php } ?>

<!--  submit order -->
<?php submitOrder() ?>
<!-- end -->

<?php

/* delete cart*/
if (@$_GET['act'] == 'delete') {
    $id = $_GET['id'];
    $cart->remove($id);
    wp_redirect(get_site_url().'/keranjang');
}

/* update cart */
if (@$_GET['act'] == 'update') {

    $quantity = $_POST['quantity'];
    $id = $_POST['id'];
    
    $post_thumbnail_id = get_post_thumbnail_id( $id );
    $filename_url = wp_get_attachment_image_src( $post_thumbnail_id, 'thumbnail' );

    //$cart->update($id, $quantity); 
    //wp_redirect(get_site_url().'/keranjang');

    //echo $id.$title.$price.get_post_meta($id, "berat", true).$filename_url[0];
    foreach ($id as $key => $value) {
        # code...
        //echo $quantity[$key]. '<br>';
        $x = $cart->update($value, @$quantity[$key], [
            'id'    => $value
        ]); 

    }
    wp_redirect(get_site_url().'/keranjang');
}
/*
    populate data - ongkir - total order
*/
if (@$_GET['act'] == 'ajax') { 

    /* show dropdon kota */
    if($_GET['ch'] == 'city') { 
        $provinsi_id = $_GET['prov_id'];

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://api.rajaongkir.com/starter/city?province=$provinsi_id",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "content-type: application/x-www-form-urlencoded",
            "key: ".get_option('guard_rajaongkir_key')
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          //echo $response;
        }

        $data = json_decode($response, true);
        for ($i=0; $i < count($data['rajaongkir']['results']); $i++) { 
            echo "<option value='".$data['rajaongkir']['results'][$i]['city_id']."'>".$data['rajaongkir']['results'][$i]['city_name']."</option>";
        }
    }
    /* end*/

    /* cek ongkir */
    if($_GET['ch'] == 'ongkir') { 
        $city_id = $_GET['city_id'];
        $pilih_kurir = $_GET['pilih_kurir'];
        $total_berat = $_GET['total_berat'];
        
        if($pilih_kurir == ''){
            $pilih_kurir = "jne";
        }

        $origin = get_option('origin');
        $destination = $city_id;
        $courier = $pilih_kurir;
        $berat = isset($total_berat) ? $total_berat : 500;

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://api.rajaongkir.com/starter/cost",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "origin=".$origin."&destination=".$destination."&weight=".$berat."&courier=".$courier."",
          CURLOPT_HTTPHEADER => array(
            "content-type: application/x-www-form-urlencoded",
            "key: ".get_option('guard_rajaongkir_key')
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          
          $data = json_decode($response, true); 
          foreach ($data['rajaongkir']['results'] as $key => $value) {

            //print_r($value);
            //code
            //costs = array
                //service
                //description
                //cost = array
                    //value
                    //etd  = waktu sampe
                    //note
            ?> 
                <table class="table custome-table">
                    <thead> 
                    <tr>
                        <th>Pilih Paket</th>
                        <th>Tarif</th>
                        <th>Lama Pengiriman</th>
                        <th>Deskripsi</th>
                    </tr>
                    </thead>
                <?php foreach ($value['costs'] as $k => $row) { ?>
                    <tbody>
                    <tr>
                        <td><input type="radio" class="service_courier" name="service" data-shipping="<?php echo $row['cost'][0]['value'] ?>" data-shop="<?php echo $cart->getAttributeTotal('price') ?>" value="<?php echo $row['service']?>" required><?php echo $row['service']?></td>
                        <td><?php echo $row['cost'][0]['value']?></td>
                        <td> 
                            <?php echo $row['cost'][0]['etd']?>
                        </td>
                        <td><?php echo $row['description']?></td>
                    </tr>
                    </tbody>
                <?php } ?>
                </table>
            <?php

          }
        }
    }
    /* end */
}

?>
