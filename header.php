<?php 
session_start(); 
/**
* Zuper Shop.
* @package WordPress 
* @subpackage zupershop V1
* @since zupershop v1
* @web kabarharian.com
* @email genthemes@gmail.com
*/
?>
<!doctype html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height" />

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dist/css/phonon.min.css" /> 

    <!--  custome  -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dist/css/theme.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="shortcut icon" href="<?php echo getOption('favicon]');?>">

    <!-- plugins -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dist/css/components/autocomplete.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dist/css/components/preloaders.css">
    

    <title>
    <?php if ( is_category() ) {
	echo 'Category Archive for &quot;'; single_cat_title(); echo '&quot; | '; bloginfo( 'name' );
	} elseif ( is_tag() ) {
		echo 'Tag Archive for &quot;'; single_tag_title(); echo '&quot; | '; bloginfo( 'name' );
	} elseif ( is_archive() ) {
		wp_title(''); echo ' Archive | '; bloginfo( 'name' );
	} elseif ( is_search() ) {
		echo 'Search for &quot;'._wp_specialchars($s).'&quot; | '; bloginfo( 'name' );
	} elseif ( is_home() || is_front_page() ) {
		bloginfo( 'name' ); echo ' | '; bloginfo( 'description' );
	}  elseif ( is_404() ) {
		echo 'Error 404 Not Found | '; bloginfo( 'name' );
	} elseif ( is_single() ) {
		wp_title('');
	} else {
		echo wp_title( ' | ', false, 'right' ); bloginfo( 'name' );
	} ?>
    </title>
  <?php $root = get_template_directory_uri(); ?> 
    <?php 
    global $cart;
    //echo get_option('google_analytic'); 
    ?>
    <?php 
    if(is_home()){
        $cart->remove('payment');
    }
    if(is_single()){
        $cart->remove('payment');
        echo get_option('fb_pixel_product');
    }
    if(is_page('keranjang')){
        if(@$_GET['do']=='payment') {
            echo get_option('fb_pixel_payment');
        } else {
            echo get_option('fb_pixel_cart');
        }   
    } 
    ?>
 
  </head>
  <body <?php body_class(); ?>>
  <!-- Panel tags go here -->
        <!-- Side Panel tags go here -->
        <div class="side-panel side-panel-left" data-expose-aside="none" data-disable="right" data-page="home" id="side-panel-example">
            <header class="header-bar">
                <button class="btn pull-right icon icon-close" data-side-panel-close="true"></button>
                <div class="pull-left">
                    <h1 class="title">Panel</h1>
                </div>
            </header>

            <div class="content">
                <ul class="list">
                    <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="padded-list"><?php echo bloginfo() ?></a></li>
                    <li><a href="<?php echo esc_url( home_url( '/contact' ) ); ?>" class="padded-list">Contact Us</a></li>
                    <li><a href="<?php echo esc_url( home_url( '/keranjang' ) ); ?>" class="padded-list">Keranjang</a></li>
                    <li><a href="<?php echo esc_url( home_url( '/konfirmasi' ) ); ?>" class="padded-list">Konfirmasi</a></li>
                </ul>
            </div>
        </div>

        <!-- Notification tags go here -->

        <!-- Dialog tags go here -->
        <div class="dialog" data-cancelable="false" id="custom-dialog">
        <form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
            <div class="content">
                <div class="padded-full">
                    <h3>Search... <i class="icon icon-info-outline"></i></h3>
                    <input type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" placeholder="Search....">
                </div>
            </div>
            <ul class="buttons">
                <li><button class="btn btn-flat primary btn-confirm" type="submit">YES</button></li>
                <li><button class="btn btn-flat negative btn-cancel" data-dialog-close="true">NO</button></li>
            </ul>
        </form>
        </div>
        <!-- Popover tags go here -->

        <!-- the home page is the default one. This page does not require to call its content because we define on its tag.-->
        <home data-page="true">

           <header class="header-bar">
                 <button class="btn icon icon-menu pull-left" data-side-panel-id="side-panel-example"></button>
                <div class="center">
                    <h1 class="title"><strong>
                    <?php if(!empty(get_option('guard_logo'))) :?>
                        <a href="<?php echo site_url(); ?>"><img src="<?php  echo get_option('guard_logo'); ?>" class="logo"></a>
                    <?php endif; ?>
                    <?php if(empty(get_option('guard_logo'))) :?>
                        <a class="navbar-brand" href="<?php echo site_url(); ?>"><?php echo bloginfo(); ?></a>
                    <?php endif ?>
                    </strong></h1>
                </div>
                <a class="btn pull-right fa fa-search" data-dialog-id="custom-dialog"></a>
            </header>
