<?php

/* theme options */
add_action( 'admin_menu', 'wpviddycpa_admin_menu');
function wpviddycpa_admin_menu() {
	add_menu_page( __( 'ZuperShop', 'Page Title' ), 'ZuperShop', 'administrator', 'setting-viddycpamovie', 'display_plugin_admin_setting', 'dashicons-format-video' );
	add_action( 'admin_init', 'register_brainmovie_lp_settings' );
}

function register_brainmovie_lp_settings() {
	//register our settings
	register_setting( 'brainmovie-lp-settings', 'guard_copyright' );
    register_setting( 'brainmovie-lp-settings', 'guard_logo' ); 
    register_setting( 'brainmovie-lp-settings', 'guard_disclaimer' );  
    register_setting( 'brainmovie-lp-settings', 'guard_rajaongkir_key' );

    register_setting( 'brainmovie-lp-settings', 'enable_sms' );
    register_setting( 'brainmovie-lp-settings', 'userkey' );
    register_setting( 'brainmovie-lp-settings', 'passkey' );
    register_setting( 'brainmovie-lp-settings', 'body_sms' );

    register_setting( 'brainmovie-lp-settings', 'whatsapp_number' );
    register_setting( 'brainmovie-lp-settings', 'whatsapp_pesan' );
    register_setting( 'brainmovie-lp-settings', 'guard_gplus' );
    register_setting( 'brainmovie-lp-settings', 'tlp' );
    register_setting( 'brainmovie-lp-settings', 'sms' );
    register_setting( 'brainmovie-lp-settings', 'bbm' );
    register_setting( 'brainmovie-lp-settings', 'line' );
    register_setting( 'brainmovie-lp-settings', 'email' );
    register_setting( 'brainmovie-lp-settings', 'from_name' );
    register_setting( 'brainmovie-lp-settings', 'subject' );
    register_setting( 'brainmovie-lp-settings', 'info' );
    register_setting( 'brainmovie-lp-settings', 'origin' );

    register_setting( 'brainmovie-lp-settings', 'google_analytic' );
    register_setting( 'brainmovie-lp-settings', 'fb_pixel_product' );
    register_setting( 'brainmovie-lp-settings', 'fb_pixel_cart' );
    register_setting( 'brainmovie-lp-settings', 'fb_pixel_payment' );

}

function display_plugin_admin_setting() {  
    $gt = new Wpviddycpa_Field(); 
	?>
<div class="wrap">

<form method="post" action="options.php">
    <?php settings_fields( 'brainmovie-lp-settings' ); ?>
    <?php do_settings_sections( 'brainmovie-lp-settings' ); ?>

    <h2>General</h2>
  

    <div class="uk-form-stacked-custome uk-form-horizontal">
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-s-it">Logo</label>
            <div class="uk-form-controls"> 
                <?php 
                    $gt->field( array(
                        'type'          => 'file', 
                        'name'          => 'guard_logo',
                        'class'         => 'uk-form-medium',  
                        'default'       => esc_attr( get_option('guard_logo') ),
                        'preview'       => esc_attr( get_option('guard_logo') ), 
                    ));
                ?> 
                <br>
                <i class="theme-option-info">Logo ukuran width 150px dan height 50px </i>
            </div>
        </div>
       <div class="uk-form-row">
            <label class="uk-form-label" for="form-s-it">Copyright</label>
            <div class="uk-form-controls"> 
                <?php 
                    $gt->field( array(
                        'type'          => 'text', 
                        'name'          => 'guard_copyright',
                        'class'         => 'uk-form-medium uk-width-1-2',
                        'default'       =>  esc_attr( get_option('guard_copyright') ),
                        'placeholder'   => 'insert copyright'
                    ));
                ?> 
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-s-it">Disclaimer</label>
            <div class="uk-form-controls"> 
                <?php 
                    $gt->field( array(
                        'type'          => 'text', 
                        'name'          => 'guard_disclaimer', 
                        'class'         => 'uk-form-medium uk-width-1-2',
                        'default'       =>  esc_attr( get_option('guard_disclaimer') ),
                        'placeholder'   => ''
                    ));
                ?> 
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-s-it">API Raja Ongkir</label>
            <div class="uk-form-controls"> 
                <?php 
                    $gt->field( array(
                        'type'          => 'text', 
                        'name'          => 'guard_rajaongkir_key',
                        'class'         => 'uk-form-medium uk-width-1-2',
                        'default'       =>  esc_attr( get_option('guard_rajaongkir_key') ),
                        'placeholder'   => ''
                    ));
                ?> 
                <br>
                <i class="theme-option-info">Untuk perhitungan ongkos kirim otomatis, bisa menggunakan API Raja Ongkir <a href="https://rajaongkir.com/" target="_blank">https://rajaongkir.com/</a></i>
            </div>
        </div> 
    </div>

    <h2>Contact</h2>
    
    <div class="uk-form-stacked-custome uk-form-horizontal">
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-s-it">Whatsapp Number</label>
            <div class="uk-form-controls"> 
                
                <?php 
                    $gt->field( array(
                        'type'          => 'text', 
                        'name'          => 'whatsapp_number',
                        'class'         => 'uk-form-medium',  
                        'default'       => esc_attr( get_option('whatsapp_number') ),
                        'placeholder'   => 'Whatsap Number' 
                    ));
                ?> 
                <br>
                <i class="theme-option-info">Format penulisan contoh: 6281250680688</i>
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-s-it">&nbsp;</label>
            <div class="uk-form-controls">  
                <?php 
                    $gt->field( array(
                        'type'          => 'textarea', 
                        'name'          => 'Whatsapp_pesan',
                        'class'         => 'uk-form-medium uk-width-1-2',  
                        'default'       => esc_attr( get_option('Whatsapp_pesan') ),
                        'placeholder'   => 'Whatsap Pesan'
                    ));
                ?> 
                <br>
                <small>Whatsap pesan</small>
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-s-it">Tlp</label>
            <div class="uk-form-controls"> 
                
                <?php 
                    $gt->field( array(
                        'type'          => 'text', 
                        'name'          => 'tlp',
                        'class'         => 'uk-form-medium',  
                        'default'       => esc_attr( get_option('tlp') ),
                        'placeholder'   => 'Telephone/Phone'
                    ));
                ?> 
            </div>
        </div> 
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-s-it">SMS</label>
            <div class="uk-form-controls"> 
                <?php 
                    $gt->field( array(
                        'type'          => 'text', 
                        'name'          => 'sms',
                        'class'         => 'uk-form-medium',  
                        'default'       => esc_attr( get_option('sms') ),
                        'placeholder'   => 'SMS'
                    ));
                ?> 
                <br>
                <i class="theme-option-info">Format penulisan contoh: 081250680688</i>
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-s-it">Line ID</label>
            <div class="uk-form-controls"> 
                
                <?php 
                    $gt->field( array(
                        'type'          => 'text', 
                        'name'          => 'line',
                        'class'         => 'uk-form-medium',  
                        'default'       => esc_attr( get_option('line') ),
                        'placeholder'   => 'Line'
                    ));
                ?> 
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-s-it">BBM</label>
            <div class="uk-form-controls"> 
                
                <?php 
                    $gt->field( array(
                        'type'          => 'text', 
                        'name'          => 'bbm',
                        'class'         => 'uk-form-medium',  
                        'default'       => esc_attr( get_option('bbm') ),
                        'placeholder'   => 'BBM Messanger'
                    ));
                ?> 
            </div>
        </div>
    </div>

    <h2>Setting</h2>
    
    <div class="uk-form-stacked-custome uk-form-horizontal">
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-s-it">Email form order</label>
            <div class="uk-form-controls">  
                <?php 
                    $gt->field( array(
                        'type'          => 'text', 
                        'name'          => 'email',
                        'class'         => 'uk-form-medium',  
                        'default'       => esc_attr( get_option('email') ),
                        'placeholder'   => 'Email address' 
                    ));
                ?> 
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-s-it">From Name</label>
            <div class="uk-form-controls">  
                <?php 
                    $gt->field( array(
                        'type'          => 'text', 
                        'name'          => 'from_name',
                        'class'         => 'uk-form-medium',  
                        'default'       => esc_attr( get_option('from_name') ),
                        'placeholder'   => 'From Name email sender' 
                    ));
                ?> 
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-s-it">Subject Email</label>
            <div class="uk-form-controls">  
                <?php 
                    $gt->field( array(
                        'type'          => 'text', 
                        'name'          => 'subject',
                        'class'         => 'uk-form-medium uk-width-1-2',  
                        'default'       => esc_attr( get_option('subject') ),
                        'placeholder'   => 'Subject Email' 
                    ));
                ?> 
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-s-it">Origin Ongokis Kirim</label>
            <div class="uk-form-controls">  
            <?php getOrigin(get_option('origin')); ?> 
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-s-it">Info nomor rekening/info tambahan</label>
            <div class="uk-form-controls">  
                <?php 
                    $gt->field( array(
                        'type'          => 'wysiwyg', 
                        'name'          => 'info',
                        'class'         => 'uk-form-medium uk-width-1-2',  
                        'default'       => ( get_option('info') ),
                        'cols'          => '20',
                        'rows'          => '8' 
                    ));
                ?> 
            </div>
        </div>
    </div>


    <h2>SMS Gateway</h2>

    <div class="uk-form-stacked-custome uk-form-horizontal">
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-s-it">Enable</label>
            <div class="uk-form-controls">  
                <input type="checkbox" name="enable_sms" value="1" <?php if(get_option('enable_sms')==1) echo 'checked'; ?>>
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-s-it">User Key</label>
            <div class="uk-form-controls">  
                <?php 
                    $gt->field( array(
                        'type'          => 'text', 
                        'name'          => 'userkey',
                        'class'         => 'uk-form-medium uk-width-1-2',  
                        'default'       => esc_attr( get_option('userkey') ),
                        'placeholder'   => 'User Key' 
                    ));
                ?> 
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-s-it">Password</label>
            <div class="uk-form-controls">  
                <?php 
                    $gt->field( array(
                        'type'          => 'text', 
                        'name'          => 'passkey',
                        'class'         => 'uk-form-medium uk-width-1-2',  
                        'default'       => esc_attr( get_option('passkey') ),
                        'placeholder'   => 'Password key' 
                    ));
                ?> 
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-s-it">Additional Body Message</label>
            <div class="uk-form-controls">  
                <?php 
                    $gt->field( array(
                        'type'          => 'text', 
                        'name'          => 'body_sms',
                        'class'         => 'uk-form-medium uk-width-1-1',  
                        'default'       => esc_attr( get_option('body_sms') ),
                        'placeholder'   => 'Additional body sms' 
                    ));
                ?> 
            </div>
        </div>
    </div>

    <h2>Tracking</h2>
    
    <div class="uk-form-stacked-custome uk-form-horizontal">
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-s-it">Google Analytic</label>
            <div class="uk-form-controls">  
                <?php 
                    $gt->field( array(
                        'type'          => 'textarea', 
                        'name'          => 'google_analytic',
                        'class'         => 'uk-form-medium uk-width-1-2',  
                        'default'       => esc_attr( get_option('google_analytic') ),
                        'cols'          => '20',
                        'rows'          => '8' 
                    ));
                ?> 
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-s-it">Facebook Pixel Product</label>
            <div class="uk-form-controls">  
                <?php 
                    $gt->field( array(
                        'type'          => 'textarea', 
                        'name'          => 'fb_pixel_product',
                        'class'         => 'uk-form-medium uk-width-1-2',  
                        'default'       => esc_attr( get_option('fb_pixel_product') ),
                        'cols'          => '20',
                        'rows'          => '8' 
                    ));
                ?> 
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-s-it">Facebook Pixel keranjang</label>
            <div class="uk-form-controls">  
                <?php 
                    $gt->field( array(
                        'type'          => 'textarea', 
                        'name'          => 'fb_pixel_cart',
                        'class'         => 'uk-form-medium uk-width-1-2',  
                        'default'       => esc_attr( get_option('fb_pixel_cart') ),
                        'cols'          => '20',
                        'rows'          => '8' 
                    ));
                ?> 
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-s-it">Facebook Pixel Payment</label>
            <div class="uk-form-controls">  
                <?php 
                    $gt->field( array(
                        'type'          => 'textarea', 
                        'name'          => 'fb_pixel_payment',
                        'class'         => 'uk-form-medium uk-width-1-2',  
                        'default'       => esc_attr( get_option('fb_pixel_payment') ),
                        'cols'          => '20',
                        'rows'          => '8' 
                    ));
                ?> 
            </div>
        </div>
    </div>
    <?php submit_button(); ?>

</form>
</div>

	<?php
}

/* end theme options */