<?php

//making the meta box (Note: meta box != custom meta field)
function wpse_add_custom_meta_box_2() { 

   add_meta_box(
       'custom_meta_box-2',       // $id
       'Meta Box Shoping',                  // $title
       'show_custom_meta_box_2',  // $callback
       'post',                  // $page
       'normal',                  // $context
       'high'                     // $priority
   );
}
add_action('add_meta_boxes', 'wpse_add_custom_meta_box_2');

function dataField() {
    $custom_meta_fields = array(  
    // tabs 1
    'tabs1' => array(
                     array(  
                          'vtr-label'=> 'Produk Habis ?',  
                          'desc'  => 'centang , jika produk ini habis', 
                          'info'  => 'fungsi ini akan mendisable tombol beli untuk produk ini',  
                          'id'    => 'habis',
                          'type'  => 'checkbox'  
                      ),  
                      array(  
                          'vtr-label'=> 'Stok Barang',  
                          'desc'  => 'contoh : Ready , Kosong, Habis, 10 pcs',  
                          'id'    => 'stok',  
                          'type'  => 'text'  
                      ),    
                      
                      array(  
                          'vtr-label'=> 'Kode Produk',  
                          'desc'  => '',  
                          'id'    => 'kode',  
                          'type'  => 'text'  
                      ),  
                      array(  
                          'vtr-label'=> 'Label',  
                          'desc'  => 'pilih , jika anda ingin memberikan label tertentu pada produk',  
                          'id'    => 'label',  
                          'type'  => 'select' ,
                           'options' => array (  
                          'one' => array (  
                              'label' => '-',  
                              'value' => ''  
                          ),  
                          'two' => array (  
                              'label' => 'Sale',  
                              'value' => 'sale'  
                          ),  
                          'three' => array (  
                              'label' => 'Best Seller',  
                              'value' => 'best'  
                          ),
                          'four' => array (  
                              'label' => 'NEW',  
                              'value' => 'new'  
                          ),
                          'five' => array (  
                              'label' => 'Limited',  
                              'value' => 'limited'  
                          )
                      )   
                      ),  
                          
                      array(  
                          'vtr-label'=> 'Harga Produk',  
                          'desc'  => '',  
                          'id'    => 'harga',  
                          'type'  => 'text'  
                      ),  
                      array(  
                          'vtr-label'=> 'Harga Diskon',  
                          'desc'  => '',  
                          'id'    => 'harga_diskon',  
                          'type'  => 'text'  
                      ),        
                      array(  
                          'vtr-label'=> 'Berat barang',  
                          'desc'  => 'satuan Gram , tulis berat barang , contoh : 100 ( artinya 100 gram)',  
                          'id'    => 'berat',  
                          'type'  => 'text'  
                      ),    
             ),
                                             
    // tabs 2                                        
    'tabs2' => array(
                    array(  
                          'vtr-label'=> 'Tampilkan Pilihan Produk',  
                          'desc'  => 'centang , jika anda ingin menampilkan pilihan', 
                          'info'  => 'contoh size S ,M ,L , XL',  
                          'id'    => 'pilihan',  
                          'type'  => 'checkbox'  
                      ),
                      array(  
                          'vtr-label'=> 'Pilihan 1',  
                          'desc'  => '',  
                          'id'    => 'pilihan1',  
                          'type'  => 'text'  
                      ),    
                      
                      array(  
                          'vtr-label'=> 'Harga Pilihan 1',  
                          'desc'  => '',   
                          'id'    => 'harga_pilihan1',  
                          'type'  => 'text'  
                      ),    
                      
                      array(  
                          'vtr-label'=> 'Pilihan 2',  
                          'desc'  => '',  
                          'id'    => 'pilihan2',  
                          'type'  => 'text'  
                      ), 
                      
                      array(  
                          'vtr-label'=> 'Harga Pilihan 2',  
                          'desc'  => '',   
                          'id'    => 'harga_pilihan2',  
                          'type'  => 'text'  
                      ),    
                      
                      array(  
                          'vtr-label'=> 'Pilihan 3',  
                          'desc'  => '',  
                          'id'    => 'pilihan3',  
                          'type'  => 'text'  
                      ),    
                      
                      array(  
                          'vtr-label'=> 'Harga Pilihan 3',  
                          'desc'  => '',   
                          'id'    => 'harga_pilihan3',  
                          'type'  => 'text'  
                      ),    
                      
                      array(  
                          'vtr-label'=> 'Pilihan 4',  
                          'desc'  => '',  
                          'id'    => 'pilihan4',  
                          'type'  => 'text'  
                      ),        
                      array(  
                          'vtr-label'=> 'Harga Pilihan 4',  
                          'desc'  => '',  
                          'id'    => 'harga_pilihan4',  
                          'type'  => 'text'  
                      ),    
              
                      array(  
                          'vtr-label'=> 'Pilihan 5',  
                          'desc'  => '',  
                          'id'    => 'pilihan5',  
                          'type'  => 'text'  
                      ),        
                      array(  
                          'vtr-label'=> 'Harga Pilihan 5',  
                          'desc'  => '',   
                          'id'    => 'harga_pilihan5',  
                          'type'  => 'text'  
                      ),
                      array(  
                          'vtr-label'=> 'Pilihan 6',  
                          'desc'  => '',  
                          'id'    => 'pilihan6',  
                          'type'  => 'text'  
                      ),        
                      array(  
                          'vtr-label'=> 'Harga Pilihan 6',  
                          'desc'  => '',   
                          'id'    => 'harga_pilihan6',  
                          'type'  => 'text'  
                      ),
                      array(  
                          'vtr-label'=> 'Pilihan 7',  
                          'desc'  => '',  
                          'id'    => 'pilihan7',  
                          'type'  => 'text'  
                      ),        
                      array(  
                          'vtr-label'=> 'Harga Pilihan 7',  
                          'desc'  => '',   
                          'id'    => 'harga_pilihan7',  
                          'type'  => 'text'  
                      )          
                      
            ),
            
    // tabs 3
    'tabs3'=> array(
                      array(  
                          'vtr-label'=> 'label 1',  
                          'desc'  => '',  
                          'id'    => 'label_1',  
                          'type'  => 'text'  
                      ),    
                      
                      array(  
                          'vtr-label'=> 'data 1',  
                          'desc'  => '',  
                          'id'    => 'data_1',  
                          'type'  => 'text'  
                      ), 
                                          array(  
                          'vtr-label'=> 'label 2',  
                          'desc'  => '',  
                          'id'    => 'label_2',  
                          'type'  => 'text'  
                      ),    
                      
                      array(  
                          'vtr-label'=> 'data 2',  
                          'desc'  => '',  
                          'id'    => 'data_2',  
                          'type'  => 'text'  
                      ), 
                                          array(  
                          'vtr-label'=> 'label 3',  
                          'desc'  => '',  
                          'id'    => 'label_3',  
                          'type'  => 'text'  
                      ),    
                      
                      array(  
                          'vtr-label'=> 'data 3',  
                          'desc'  => '',  
                          'id'    => 'data_3',  
                          'type'  => 'text'  
                      ), 
                                              array(  
                          'vtr-label'=> 'label 4',  
                          'desc'  => '',  
                          'id'    => 'label_4',  
                          'type'  => 'text'  
                      ),    
                      
                      array(  
                          'vtr-label'=> 'data 4',  
                          'desc'  => '',  
                          'id'    => 'data_4',  
                          'type'  => 'text'  
                      ), 
                                              array(  
                          'vtr-label'=> 'label 5',  
                          'desc'  => '',  
                          'id'    => 'label_5',  
                          'type'  => 'text'  
                      ),    
                      
                      array(  
                          'vtr-label'=> 'data 5',  
                          'desc'  => '',  
                          'id'    => 'data_5',  
                          'type'  => 'text'  
                      ), 
                      array(  
                          'vtr-label'=> 'label 6',  
                          'desc'  => '',  
                          'id'    => 'label_6',  
                          'type'  => 'text'  
                      ),    
                      
                      array(  
                          'vtr-label'=> 'data 6',  
                          'desc'  => '',  
                          'id'    => 'data_6',  
                          'type'  => 'text'  
                      ), 
                      array(  
                          'vtr-label'=> 'label 7',  
                          'desc'  => '',  
                          'id'    => 'label_7',  
                          'type'  => 'text'  
                      ),    
                      
                      array(  
                          'vtr-label'=> 'data 7',  
                          'desc'  => '',  
                          'id'    => 'data_7',  
                          'type'  => 'text'  
                      ), 
                      
                ),
    // tabs 4
    'tabs4'=> array(
                      array(  
                          'vtr-label'=> 'info tambahan',  
                          'desc'  => 'tulis info tambahan',  
                          'id'    => 'info_tambahan',  
                          'type'  => 'textarea'  
                      )
                      
                ),
        // tabs 4
    'tabs5'=> array(
                    array(  
                          'vtr-label'=> 'berat volumetrik',  
                          'desc'  => 'centang , jika produk ini menggunakan berat volumetrik', 
                          'info'  => 'info lengkap , baca disini http://theme-id.com/virtacart/berat',  
                          'id'    => 'volumetrik',  
                          'type'  => 'checkbox'  
                      ),
                      array(  
                          'vtr-label'=> 'Panjang',  
                          'desc'  => 'satuan CM , tulis hanya angka saja , contoh : 50 ',  
                          'id'    => 'panjang',  
                          'type'  => 'text'  
                      ), 
                      array(  
                          'vtr-label'=> 'Lebar',  
                          'desc'  => 'satuan CM , tulis hanya angka saja , contoh : 50 ',   
                          'id'    => 'lebar',  
                          'type'  => 'text'  
                      ),
                      array(  
                          'vtr-label'=> 'Tinggi',  
                          'desc'  => 'satuan CM , tulis hanya angka saja , contoh : 50 ',    
                          'id'    => 'tinggi',  
                          'type'  => 'text'  
                      ),    
                      
                )
    );  

	return $custom_meta_fields;
}

function show_custom_meta_box_2() {
    global $post;
    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'inc/class-wpviddycpa-field.php'; 
	$gt = new Wpviddycpa_Field(); 
    wp_nonce_field( basename( __FILE__ ), 'wpse_our_nonce' );

    ?>
 	<ul class="uk-tab" data-uk-tab="{connect:'#my-id'}">
        <li class="uk-active"><a href="">General</a></li>
        <li><a href="">Pilihan</a></li>
        <li><a href="">Data Tambahan</a></li> 
        <li><a href="">Info Tambahan</a></li> 
        <li><a href="">Berat Volumetrik</a></li> 
    </ul>
    <ul id="my-id" class="uk-switcher uk-margin">
        <?php
        foreach (dataField() as $key => $fields) {
         echo '<li id="'.$key.'" class="uk-form uk-form-horizontal">';
         //echo '<table class="vtr-table">'; 
         foreach ($fields as $field){
             $meta = get_post_meta($post->ID, $field['id'], true);  
             echo '<div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-it">'.$field['vtr-label'].'</label> 
                    <div class="uk-form-controls">'; 
             switch($field['type']) {  
                                  // text  
                                  case 'text':  
                                      echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'"/> 
                                          <br /><span class="description">'.$field['desc'].'</span>';  
                                  break;  
                                      // textarea  
                                  case 'textarea':  
                                      echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" cols="60" rows="4">'.$meta.'</textarea> 
                                          <br /><span class="description">'.$field['desc'].'</span>';  
                                  break;  
                                      // checkbox  
                                  case 'checkbox':  
                                      echo '<input type="checkbox" name="'.$field['id'].'" id="'.$field['id'].'" ',$meta ? ' checked="checked"' : '','/> 
                                          <label for="'.$field['id'].'">'.$field['desc'].'</label>';
                                          echo '<br /><span class="description">'.$field['info'].'</span>';    
                                  break;  
                                  
                                      // select  
                                  case 'select':  
                                      echo '<select name="'.$field['id'].'" id="'.$field['id'].'">';  
                                      foreach ($field['options'] as $option) {  
                                          echo '<option', $meta == $option['value'] ? ' selected="selected"' : '', ' value="'.$option['value'].'">'.$option['label'].'</option>';  
                                      }  
                                      echo '</select><br /><span class="description">'.$field['desc'].'</span>';  
                                  break;  
                                    } //end switch 
                    echo '</div></div>'; 
         } // end foreach ($fields as $field){
             //echo '</table>'; // end table
         echo '</li>';
     }  
        ?>
    </ul>
    <?php
}

function sanitizeStringForUrl($string){
    $string = str_replace(array('_'),array(' '),$string);
    return ucwords(strtolower($string));
}

//now we are saving the data
function wpse_save_meta_fields( $post_id ) {

$options = get_option('wpviddycpa-option'); 
$custom_post_type = !empty($options['custome-post-type']) ? $options['custome-post-type'] : '';
$slug_custome_post =  !empty($options['slug-custome-post-type']) ? $options['slug-custome-post-type'] : ''; 

$plugin_meta = 'wpviddycpa-meta';

  // verify nonce
  if (!isset($_POST['wpse_our_nonce']) || !wp_verify_nonce($_POST['wpse_our_nonce'], basename(__FILE__)))
      return 'nonce not verified';

  // check autosave
  if ( wp_is_post_autosave( $post_id ) )
      return 'autosave';

  //check post revision
  if ( wp_is_post_revision( $post_id ) )
      return 'revision';

  if ( 'page' == $_POST['post_type'] ) {
    if ( !current_user_can( 'edit_page', $post_id ) )
    return;
  } else {
    if ( !current_user_can( 'edit_post', $post_id ) )
    return;
  }

  // loop through fields and save the data  
    foreach (dataField() as $key => $fields) {
          foreach ($fields as $field) {  
              $old = get_post_meta($post_id, $field['id'], true);  
              $new = $_POST[$field['id']]; 
              if ($new && $new != $old) {  
                  update_post_meta($post_id, $field['id'], $new);  
              } elseif ('' == $new && $old) {  
                  delete_post_meta($post_id, $field['id'], $old);  
              }  
          } // end foreach 
    }// end foreach

}
add_action( 'save_post', 'wpse_save_meta_fields' );
add_action( 'new_to_publish', 'wpse_save_meta_fields' );

?>