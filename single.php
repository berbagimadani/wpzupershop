<?php
/**
 * Gen Themes Display.
 * @package WordPress
 * @subpackage Genthemes V1
 * @since genthemes v1
 * @web genthemes.net
 * @email genthemes@gmail.com
 */

get_header(); //deleteCookie(); 
//unset($_COOKIE['zupershop_setcookie_cart']);
//setcookie( 'zupershop_setcookie_cart', '', time() - ( 86400 * 30 ), COOKIEPATH, COOKIE_DOMAIN  );
?>
<?php if (have_posts()) : ?>  
<?php while ( have_posts() ) : the_post(); ?>
	
	 <div class="content">
                <div class="padded-full responsive-desk">
                
                    <div class="row">
                        <h1><?php the_title() ?></h1>
                        <div class="phone-12 tablet-12 column" style=""> 
                            <?php
                                if( get_post_meta($post->ID, "label", true) == "best") {
                                    $label = "Best Seller";
                                }
                                if( get_post_meta($post->ID, "label", true) == "sale") {
                                    $label = "Sale";
                                }
                                if( get_post_meta($post->ID, "label", true) == "new") {
                                    $label = "New";
                                }
                                if( get_post_meta($post->ID, "label", true) == "limited") {
                                    $label = "Limited";
                                }
                            ?>
                            <?php if( get_post_meta($post->ID, "habis", true) == false) { ?>
                            <div class="ribbon <?php echo get_post_meta($post->ID, "label", true); ?>"><span><?php echo $label; ?></span></div>
                            <?php } ?>
                            <?php if( get_post_meta($post->ID, "habis", true) ) { ?>
                            <div class="ribbon sold"><span>Habis</span></div>
                            <?php } ?>

                            <div class="custome-box-images">
                                <?php   
                                    echo the_post_thumbnail('large', array('class' => 'wpshopguardian-res-img'));  
                                ?> 
                            </div>
                        </div> 
                        <form action="<?php echo get_permalink(); ?>" method="post">
                        <input type="hidden" name="action" value="submit" />
                        <div class="phone-<?php if(get_post_meta($post->ID, "label_1", true)) echo '6';  else  echo '12';  ?> tablet-<?php if(get_post_meta($post->ID, "label_1", true)) echo '6';  else  echo '12';  ?> column">
                            <div class="custome-list">
                                <ul class="list"> 
                                    <li class="item-expanded"> 
                                        <a class="pull-left">Kode</a>
                                        <div class="item-content">
                                            <span class="title"><strong><?php echo get_post_meta($post->ID, "kode", true); ?></strong></span> 
                                        </div>
                                    </li>
                                    <li class="item-expanded"> 
                                        <a class="pull-left">Harga</a>
                                        <div class="item-content">
                                            <span class="title"><strong><?php echo currency(get_post_meta($post->ID, "harga_diskon", true)); ?> <span class="coret"><?php echo currency(get_post_meta($post->ID, "harga", true)); ?></span></strong></span> 
                                        </div>
                                    </li>
                                    <li class="item-expanded"> 
                                        <a class="pull-left">Anda Hemat</a> 
                                        <div class="item-content">
                                            <span class="title"><strong><?php wpshopguardian_diskon() ?></strong></span> 
                                        </div>
                                    </li> 
                                    <li class="item-expanded"> 
                                        <a class="pull-left">Stock</a>
                                        <div class="item-content">
                                            <span class="title"><strong><?php echo get_post_meta($post->ID, "stok", true); ?></strong></span> 
                                        </div>
                                    </li>
                                    <li class="item-expanded"> 
                                        <a class="pull-left">Berat</a> 
                                        <div class="item-content">
                                            <span class="title"><?php echo get_post_meta($post->ID, "berat", true); ?></span> 
                                        </div>
                                    </li>
                                    <?php if( get_post_meta($post->ID, "pilihan", true) ) { ?>
                                    <li class="item-expanded"> 
                                        <a class="pull-left">Pilihan</a>  
                                        <div class="item-content"> 
                                            <!--<button class="btn pull-right icon icon-expand-more"></button>-->
                                                <select class="title custome-dropdown" name="pricing_choose">
                                                    <?php if( get_post_meta($post->ID, "pilihan1", true) ) { ?>
                                                    <option value="<?php echo get_post_meta($post->ID, "harga_pilihan1", true); ?>"><?php echo get_post_meta($post->ID, "pilihan1", true); ?> @<?php echo get_post_meta($post->ID, "harga_pilihan1", true); ?></option>
                                                    <?php } ?>
                                                    <?php if( get_post_meta($post->ID, "pilihan2", true) ) { ?>
                                                    <option value="<?php echo get_post_meta($post->ID, "harga_pilihan2", true); ?>"><?php echo get_post_meta($post->ID, "pilihan2", true); ?> @<?php echo get_post_meta($post->ID, "harga_pilihan2", true); ?></option></option>
                                                    <?php } ?>
                                                    <?php if( get_post_meta($post->ID, "pilihan3", true) ) { ?>
                                                    <option value="<?php echo get_post_meta($post->ID, "harga_pilihan3", true); ?>"><?php echo get_post_meta($post->ID, "pilihan3", true); ?> @<?php echo get_post_meta($post->ID, "harga_pilihan3", true); ?></option></option>
                                                    <?php } ?>
                                                    <?php if( get_post_meta($post->ID, "pilihan4", true) ) { ?>
                                                    <option value="<?php echo get_post_meta($post->ID, "harga_pilihan4", true); ?>"><?php echo get_post_meta($post->ID, "pilihan4", true); ?> @<?php echo get_post_meta($post->ID, "harga_pilihan4", true); ?></option></option>
                                                    <?php } ?>
                                                    <?php if( get_post_meta($post->ID, "pilihan5", true) ) { ?>
                                                    <option value="<?php echo get_post_meta($post->ID, "harga_pilihan5", true); ?>"><?php echo get_post_meta($post->ID, "pilihan5", true); ?> @<?php echo get_post_meta($post->ID, "harga_pilihan5", true); ?></option></option>
                                                    <?php } ?>
                                                    <?php if( get_post_meta($post->ID, "pilihan6", true) ) { ?>
                                                    <option value="<?php echo get_post_meta($post->ID, "harga_pilihan6", true); ?>"><?php echo get_post_meta($post->ID, "pilihan6", true); ?> @<?php echo get_post_meta($post->ID, "harga_pilihan6", true); ?></option></option>
                                                    <?php } ?>
                                                    <?php if( get_post_meta($post->ID, "pilihan7", true) ) { ?>
                                                    <option value="<?php echo get_post_meta($post->ID, "harga_pilihan7", true); ?>"><?php echo get_post_meta($post->ID, "pilihan7", true); ?> @<?php echo get_post_meta($post->ID, "harga_pilihan7", true); ?></option></option>
                                                    <?php } ?>
                                                </select> 
                                        </div>
                                    </li> 
                                    <?php } ?>

                                </ul>
                            </div>
                        </div>

                        <!-- right additional -->
                        <?php if(  get_post_meta($post->ID, "label_1", true) ) { ?>
                        <div class="phone-6 tablet-6 column">
                            <div class="custome-list">
                                <ul class="list"> 
                                    <?php if(get_post_meta($post->ID, "label_1", true)) {?>
                                    <li class="item-expanded"> 
                                        <a class="pull-left"><?php echo get_post_meta($post->ID, "label_1", true); ?></a>
                                        <div class="item-content">
                                            <span class="title"><strong><?php echo get_post_meta($post->ID, "data_1", true); ?></strong></span> 
                                        </div>
                                    </li>
                                    <?php } ?>
                                    <?php if(get_post_meta($post->ID, "label_2", true)) {?>
                                    <li class="item-expanded"> 
                                        <a class="pull-left"><?php echo get_post_meta($post->ID, "label_2", true); ?></a>
                                        <div class="item-content">
                                            <span class="title"><strong><?php echo get_post_meta($post->ID, "data_2", true); ?></strong></span> 
                                        </div>
                                    </li>
                                    <?php } ?>
                                    <?php if(get_post_meta($post->ID, "label_3", true)) {?>
                                    <li class="item-expanded"> 
                                        <a class="pull-left"><?php echo get_post_meta($post->ID, "label_3", true); ?></a>
                                        <div class="item-content">
                                            <span class="title"><strong><?php echo get_post_meta($post->ID, "data_3", true); ?></strong></span> 
                                        </div>
                                    </li>
                                    <?php } ?>
                                    <?php if(get_post_meta($post->ID, "label_4", true)) {?>
                                    <li class="item-expanded"> 
                                        <a class="pull-left"><?php echo get_post_meta($post->ID, "label_4", true); ?></a>
                                        <div class="item-content">
                                            <span class="title"><strong><?php echo get_post_meta($post->ID, "data_4", true); ?></strong></span> 
                                        </div>
                                    </li>
                                    <?php } ?>
                                    <?php if(get_post_meta($post->ID, "label_5", true)) {?>
                                    <li class="item-expanded"> 
                                        <a class="pull-left"><?php echo get_post_meta($post->ID, "label_5", true); ?></a>
                                        <div class="item-content">
                                            <span class="title"><strong><?php echo get_post_meta($post->ID, "data_5", true); ?></strong></span> 
                                        </div>
                                    </li>
                                    <?php } ?>
                                    <?php if(get_post_meta($post->ID, "label_6", true)) {?>
                                    <li class="item-expanded"> 
                                        <a class="pull-left"><?php echo get_post_meta($post->ID, "label_6", true); ?></a>
                                        <div class="item-content">
                                            <span class="title"><strong><?php echo get_post_meta($post->ID, "data_6", true); ?></strong></span> 
                                        </div>
                                    </li>
                                    <?php } ?>
                                    <?php if(get_post_meta($post->ID, "label_7", true)) {?>
                                    <li class="item-expanded"> 
                                        <a class="pull-left"><?php echo get_post_meta($post->ID, "label_7", true); ?></a>
                                        <div class="item-content">
                                            <span class="title"><strong><?php echo get_post_meta($post->ID, "data_7", true); ?></strong></span> 
                                        </div>
                                    </li> 
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                        <!-- end right additional -->

                        <?php } ?>
                        <?php if( get_post_meta($post->ID, "habis", true) == false ) { ?>
                        <div class="phone-12 tablet-12 column"> 
                            <button type="submit" class="btn custome-btn-order fit-parent primary">
                                <i class="fa fa-shopping-cart" aria-hidden="true"></i> Beli Sekarang
                            </button>
                            
                        </div>
                        <?php } ?>
                        </div>
                        </form>
 
                     <a href="<?php echo get_permalink(); ?>?act=cart">
                     </a>
                    <div class="padded-bottom"></div>

<?php endwhile; ?>
<?php endif; ?>

<?php   
    
    $post_thumbnail_id = get_post_thumbnail_id( get_the_id() );
    $filename_url = wp_get_attachment_image_src( $post_thumbnail_id, 'thumbnail' );

    if (isset($_POST['action'])) {
        //echo 'Success'; 
        $price = null;
        if(isset($_POST['pricing_choose'])){
            $price = $_POST['pricing_choose'];
        } else {
            $price = get_post_meta($post->ID, "harga_diskon", true);
        }
        $cart->add(get_the_id(), 1, [
            'id'    => get_the_id(),
            'title' => get_the_title(),
            'price' => $price, 
            'berat' => get_post_meta($post->ID, "berat", true),
            'images'  => @$filename_url[0]
        ]);
        wp_redirect(get_site_url().'/keranjang');
        
    } 
?>
<?php get_footer(); ?>